package by.bsuir.trainschedule;

import by.bsuir.trainschedule.entity.User;
import by.bsuir.trainschedule.entity.enums.RoleEnum;
import by.bsuir.trainschedule.service.UserService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public class UserServiceTest {

    private static UserService userService;

    @BeforeClass
    public static void initContext() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:/datasource-tx-jpa.xml");
        ctx.refresh();
        userService = ctx.getBean("userService", UserService.class);
    }

    @Test
    public void testUserFindAll() {
        List<User> userList = userService.findAll();
        int expected = 2;
        Assert.assertEquals(expected, userList.size());
    }

    @Test
    public void addUserTest() {
        User user = new User();
        user.setLogin("yura");
        user.setPassword("yura");
        user.setEmail("1@gmail.com");
        user.setRole(RoleEnum.ANONYMOUS);
        userService.save(user);
        int expected = 3;
        Assert.assertEquals(expected, userService.findAll().size());
        Assert.assertEquals(user, userService.findById(user.getId()));
    }
}
