CREATE TABLE user_data (
	id INT NOT NULL AUTO_INCREMENT,
	login VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	role INT NOT NULL,
	UNIQUE uq_user_data_1 (login),
	UNIQUE uq_user_data_2 (email),
	PRIMARY KEY (id)
);
	
CREATE TABLE station (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50),
	PRIMARY KEY (id)
);

CREATE TABLE train (
	id INT NOT NULL AUTO_INCREMENT,
	train_number VARCHAR(10) NOT NULL,
	name VARCHAR(80) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE uq_train_1 (train_number)
);

CREATE TABLE ticket_price (
	id INT NOT NULL AUTO_INCREMENT,
	ticket_type VARCHAR(40) NOT NULL,
	start_price INT NOT NULL,
	unit_price INT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE route (
	id INT NOT NULL AUTO_INCREMENT,
	train_id INT NOT NULL,
	station_id INT NOT NULL,
	time_shift INT NOT NULL,
	PRIMARY KEY(id),
	UNIQUE uq_route_1 (train_id, station_id),
	CONSTRAINT fk_route_train FOREIGN KEY (train_id)
		REFERENCES train (id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_route_station FOREIGN KEY (station_id)
		REFERENCES station (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE ticket_in_train (
	id INT NOT NULL AUTO_INCREMENT,
	train_id INT NOT NULL,
	ticket_id INT NOT NULL,
	ticket_count INT DEFAULT 0,
	PRIMARY KEY (id),
	UNIQUE uq_ticket_in_train_1 (train_id, ticket_id),
	CONSTRAINT fk_ticket_in_train_train FOREIGN KEY (train_id)
		REFERENCES train (id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_ticket_in_train_ticket_price FOREIGN KEY (ticket_id)
		REFERENCES ticket_price (id) ON DELETE CASCADE ON UPDATE CASCADE	
);

CREATE TABLE schedule (
	id INT NOT NULL AUTO_INCREMENT,
	train_id INT NOT NULL,
	day_of_week VARCHAR(20) NOT NULL,
	departure_time TIME NOT NULL,
	active BOOLEAN DEFAULT TRUE,
	PRIMARY KEY (id),
	CONSTRAINT fk_schedule_train FOREIGN KEY (train_id)
		REFERENCES train (id) ON DELETE CASCADE ON UPDATE CASCADE
);