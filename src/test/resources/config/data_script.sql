INSERT INTO `station` (`name`) VALUES ('Минск-Пассажирский');
INSERT INTO `station` (`name`) VALUES ('Барановичи');
INSERT INTO `station` (`name`) VALUES ('Ивацевичи');
INSERT INTO `station` (`name`) VALUES ('Бронная гора');
INSERT INTO `station` (`name`) VALUES ('Берёза-город');
INSERT INTO `station` (`name`) VALUES ('Оранчицы');
INSERT INTO `station` (`name`) VALUES ('Жабинка');
INSERT INTO `station` (`name`) VALUES ('Брест-центральный');


INSERT INTO `ticket_price` (`ticket_type`, `start_price`, `unit_price`) VALUES ('THIRD_CLASS', '5000', '200');
INSERT INTO `ticket_price` (`ticket_type`, `start_price`, `unit_price`) VALUES ('BERTH', '7000', '300');
INSERT INTO `ticket_price` (`ticket_type`, `start_price`, `unit_price`) VALUES ('SECOND_CLASS', '10000', '300');
INSERT INTO `ticket_price` (`ticket_type`, `start_price`, `unit_price`) VALUES ('FIRST_CLASS', '20000', '500');
INSERT INTO `ticket_price` (`ticket_type`, `start_price`, `unit_price`) VALUES ('SEAT', '7000', '300');



INSERT INTO `user_data` (`login`, `password`, `email`, `role`) VALUES ('admin', 'admin', 'admin@gmail.com', '1');

INSERT INTO `train` (`train_number`, `name`) VALUES ('601', 'Минск-Пассажирский - Брест-центральный');
INSERT INTO `train` (`train_number`, `name`) VALUES ('602', 'Минск-Пассажирский - Брест-центральный 2');
INSERT INTO `train` (`train_number`, `name`) VALUES ('603', 'Минск-Пассажирский - Брест-центральный 3');


INSERT INTO `ticket_in_train` (`train_id`, `ticket_id`, `ticket_count`) VALUES (1, 1, 100);
INSERT INTO `ticket_in_train` (`train_id`, `ticket_id`, `ticket_count`) VALUES (1, 2, 200);
INSERT INTO `ticket_in_train` (`train_id`, `ticket_id`, `ticket_count`) VALUES (2, 3, 200);
INSERT INTO `ticket_in_train` (`train_id`, `ticket_id`, `ticket_count`) VALUES (3, 4, 200);

INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 1, 0);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 2, 3600);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 3, 4400);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 4, 5000);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 6, 10000);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (1, 8, 14000);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (2, 1, 0);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (2, 8, 13000);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (3, 8, 0);
INSERT INTO `route` (`train_id`, `station_id`, `time_shift`) VALUES (3, 1, 12000);


INSERT INTO `schedule` (`train_id`, `day_of_week`, `departure_time`) VALUES (1, 'FRIDAY', '08:20:00');
INSERT INTO `schedule` (`train_id`, `day_of_week`, `departure_time`) VALUES (1, 'MONDAY', '10:40:00');
INSERT INTO `schedule` (`train_id`, `day_of_week`, `departure_time`) VALUES (2, 'SUNDAY', '17:20:00');
INSERT INTO `schedule` (`train_id`, `day_of_week`, `departure_time`) VALUES (3, 'MONDAY', '18:30:00');