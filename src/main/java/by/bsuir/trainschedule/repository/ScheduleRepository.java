package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.Schedule;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
public interface ScheduleRepository extends CrudRepository<Schedule, Long> {
    List<Schedule> findByDayOfWeek(DayEnum day);
    List<Schedule> findByTrainId(Long id);
}
