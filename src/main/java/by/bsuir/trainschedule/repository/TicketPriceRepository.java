package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.TicketPrice;
import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yura on 10.04.2016.
 */
public interface TicketPriceRepository extends CrudRepository<TicketPrice, Long> {
    TicketPrice findOneTicketPriceByTicketType(TicketTypeEnum ticketTypeEnum);
}
