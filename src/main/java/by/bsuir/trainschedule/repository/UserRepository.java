package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yura on 10.04.2016.
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findFirstByLogin(String login);
    User findFirstByEmail(String email);
}
