package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.Route;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
public interface RouteRepository extends CrudRepository<Route, Long> {
    List<Route> findByTrainIdOrderByTimeShiftAsc(Long id);
    List<Route> findByStationId(Long id);
    Route findByTrainIdAndStationId(Long trainId, Long stationId);
}
