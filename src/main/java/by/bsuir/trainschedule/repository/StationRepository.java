package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.Station;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yura on 10.04.2016.
 */
public interface StationRepository extends CrudRepository<Station, Long> {
    Station findByName(String name);
}
