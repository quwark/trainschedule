package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.Train;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
public interface TrainRepository extends CrudRepository<Train, Long> {
    @Query(value = "SELECT * FROM train INNER JOIN route ON train.id = route.train_id" +
            " WHERE route.station_id = ?1" +
            " AND train.id IN ( SELECT train.id FROM train INNER JOIN route ON train.id = route.train_id" +
            " WHERE route.station_id = ?2) GROUP BY train.id",
    nativeQuery = true)
    List<Train> findTrainByStations(Long startStationId, Long endStationId);
    Train findOneByTrainNumber(String trainNumber);
}
