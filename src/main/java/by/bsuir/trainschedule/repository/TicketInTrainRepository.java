package by.bsuir.trainschedule.repository;

import by.bsuir.trainschedule.entity.TicketInTrain;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
public interface TicketInTrainRepository extends CrudRepository<TicketInTrain, Long> {
    List<TicketInTrain> findByTrainId(Long id);
}
