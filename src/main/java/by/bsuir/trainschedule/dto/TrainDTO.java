package by.bsuir.trainschedule.dto;

import by.bsuir.trainschedule.entity.Train;
import org.joda.time.LocalTime;

import java.util.List;


/**
 * Created by Yura on 04.05.2016.
 */
public class TrainDTO {

    private Train train;

    private String departureStation;
    private String arrivalStation;
    private String departureTime;
    private String arrivalTime;
    private List<Integer> days;
    private int thirdClassTicketCount;
    private int secondClassTicketCount;
    private int firstClassTicketCount;
    private int berthTicketCount;
    private int seatTicketCount;

    public int getThirdClassTicketCount() {
        return thirdClassTicketCount;
    }

    public void setThirdClassTicketCount(int thirdClassTicketCount) {
        this.thirdClassTicketCount = thirdClassTicketCount;
    }

    public int getSecondClassTicketCount() {
        return secondClassTicketCount;
    }

    public void setSecondClassTicketCount(int secondClassTicketCount) {
        this.secondClassTicketCount = secondClassTicketCount;
    }

    public int getFirstClassTicketCount() {
        return firstClassTicketCount;
    }

    public void setFirstClassTicketCount(int firstClassTicketCount) {
        this.firstClassTicketCount = firstClassTicketCount;
    }

    public int getBerthTicketCount() {
        return berthTicketCount;
    }

    public void setBerthTicketCount(int berthTicketCount) {
        this.berthTicketCount = berthTicketCount;
    }

    public int getSeatTicketCount() {
        return seatTicketCount;
    }

    public void setSeatTicketCount(int seatTicketCount) {
        this.seatTicketCount = seatTicketCount;
    }

    public List<Integer> getDays() {
        return days;
    }

    public void setDays(List<Integer> days) {
        this.days = days;
    }
    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
