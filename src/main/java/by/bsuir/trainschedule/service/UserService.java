package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.User;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
public interface UserService {
    List<User> findAll();
    User findById(Long id);
    User findByLogin(String login);
    User findByEmail(String email);
    User save(User user);
    void remove(User user);
    boolean isLoginExist(String login);
    boolean isEmailExist(String email);
}
