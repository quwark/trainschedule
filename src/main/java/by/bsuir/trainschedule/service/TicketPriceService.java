package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.TicketPrice;
import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface TicketPriceService {
    List<TicketPrice> findAll();
    TicketPrice findById(Long id);
    TicketPrice findTicketPriceByTicketType(TicketTypeEnum ticketTypeEnum);
    TicketPrice save(TicketPrice ticketPrice);
    void remove(TicketPrice ticketPrice);
}
