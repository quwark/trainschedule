package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.TicketInTrain;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface TicketInTrainService {
    List<TicketInTrain> findAll();
    List<TicketInTrain> findByTrainId(Long id);
    TicketInTrain findById(Long id);
    TicketInTrain save(TicketInTrain ticketInTrain);
    void remove(TicketInTrain ticketInTrain);
}
