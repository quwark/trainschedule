package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.Schedule;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import org.joda.time.LocalTime;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface ScheduleService  {
    List<Schedule> findAll();
    Schedule findById(Long id);
    List<Schedule> findByDayOfWeek(DayEnum day);
    List<Schedule> findByTime(LocalTime start, LocalTime end);
    List<Schedule> findByDayOFWeekAndTime(DayEnum day, LocalTime start, LocalTime end);
    List<Schedule> findByTrainId(Long id);
    Schedule save(Schedule schedule);
    void remove(Schedule schedule);
}
