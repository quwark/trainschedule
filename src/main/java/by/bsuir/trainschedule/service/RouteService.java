package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.Route;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface RouteService {
    List<Route> findAll();
    Route findById(Long id);
    List<Route> findByTrainId(Long id);
    List<Route> findByStationId(Long id);
    Route findByTrainAndStationId(Long trainId, Long stationId);
    Route save(Route route);
    void remove(Route route);
}
