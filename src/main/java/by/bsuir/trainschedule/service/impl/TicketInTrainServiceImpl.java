package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.TicketInTrain;
import by.bsuir.trainschedule.repository.TicketInTrainRepository;
import by.bsuir.trainschedule.service.TicketInTrainService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
@Service("ticketInTrainService")
@Repository
@Transactional
public class TicketInTrainServiceImpl implements TicketInTrainService {

    @Autowired
    private TicketInTrainRepository ticketInTrainRepository;

    @Override
    @Transactional(readOnly = true)
    public List<TicketInTrain> findAll() {
        return Lists.newArrayList(ticketInTrainRepository.findAll());
    }

    @Override
    public List<TicketInTrain> findByTrainId(Long id) {
        return ticketInTrainRepository.findByTrainId(id);
    }

    @Override
    @Transactional(readOnly = true)
    public TicketInTrain findById(Long id) {
        return ticketInTrainRepository.findOne(id);
    }

    @Override
    public TicketInTrain save(TicketInTrain ticketInTrain) {
        return ticketInTrainRepository.save(ticketInTrain);
    }

    @Override
    public void remove(TicketInTrain ticketInTrain) {
        ticketInTrainRepository.delete(ticketInTrain);
    }
}
