package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.Route;
import by.bsuir.trainschedule.repository.RouteRepository;
import by.bsuir.trainschedule.service.RouteService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
@Service("routeService")
@Repository
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Route> findAll() {
        return Lists.newArrayList(routeRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public Route findById(Long id) {
        return routeRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Route> findByTrainId(Long id) {
        return routeRepository.findByTrainIdOrderByTimeShiftAsc(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Route> findByStationId(Long id) {
        return routeRepository.findByStationId(id);
    }

    @Override
    public Route findByTrainAndStationId(Long trainId, Long stationId) {
        return routeRepository.findByTrainIdAndStationId(trainId, stationId);
    }

    @Override
    public Route save(Route route) {
        return routeRepository.save(route);
    }

    @Override
    public void remove(Route route) {
        routeRepository.delete(route);
    }
}
