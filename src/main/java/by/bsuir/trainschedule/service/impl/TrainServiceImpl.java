package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.Route;
import by.bsuir.trainschedule.entity.Train;
import by.bsuir.trainschedule.repository.RouteRepository;
import by.bsuir.trainschedule.repository.TrainRepository;
import by.bsuir.trainschedule.service.TrainService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by Yura on 11.04.2016.
 */
@Service("trainService")
@Repository
@Transactional
public class TrainServiceImpl implements TrainService {

    @Autowired
    private TrainRepository trainRepository;
    @Autowired
    private RouteRepository routeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Train> findAll() {
        return Lists.newArrayList(trainRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Train> findTrainByStations(Long startStation, Long endStation) {
        if(startStation.equals(endStation)) {
            return new ArrayList<>();
        }
        return trainRepository.findTrainByStations(startStation, endStation);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Train> findTrainByStationsDirect(Long startStation, Long endStation) {
        if(startStation.equals(endStation)) {
            return new ArrayList<>();
        }

        List<Train> trains = trainRepository.findTrainByStations(startStation, endStation);

        trains.removeIf(train -> {
            List<Route> routes = routeRepository.findByTrainIdOrderByTimeShiftAsc(train.getId());
            int startValue = 0;
            int endValue = 0;

            for(Route route : routes) {
                if(route.getStationId().equals(startStation)) {
                    startValue = route.getTimeShift();
                } else if(route.getStationId().equals(endStation)) {
                    endValue = route.getTimeShift();
                }
            }
            return !(endValue > startValue);
        });
        return trains;
    }

    @Override
    public Train findByTrainNumber(String trainNumber) {
        return trainRepository.findOneByTrainNumber(trainNumber);
    }

    @Override
    @Transactional(readOnly = true)
    public Train findById(Long id) {
        return trainRepository.findOne(id);
    }

    @Override
    public Train save(Train user) {
        return trainRepository.save(user);
    }

    @Override
    public void remove(Train user) {
        trainRepository.delete(user);
    }
}
