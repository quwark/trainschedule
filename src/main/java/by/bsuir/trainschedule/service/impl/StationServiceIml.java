package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.Station;
import by.bsuir.trainschedule.repository.StationRepository;
import by.bsuir.trainschedule.service.StationService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
@Service("stationService")
@Repository
@Transactional
public class StationServiceIml implements StationService {

    @Autowired
    private StationRepository stationRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Station> findAll() {
        return Lists.newArrayList(stationRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public Station findById(Long id) {
        return stationRepository.findOne(id);
    }

    @Override
    public Station findByStationName(String name) {
        return stationRepository.findByName(name);
    }

    @Override
    public Station save(Station station) {
        return stationRepository.save(station);
    }

    @Override
    public void remove(Station station) {
        stationRepository.delete(station);
    }
}
