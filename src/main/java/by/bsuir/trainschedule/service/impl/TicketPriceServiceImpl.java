package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.TicketPrice;
import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;
import by.bsuir.trainschedule.repository.TicketPriceRepository;
import by.bsuir.trainschedule.service.TicketPriceService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
@Service("ticketPriceService")
@Repository
@Transactional
public class TicketPriceServiceImpl implements TicketPriceService {

    @Autowired
    private TicketPriceRepository ticketPriceRepository;

    @Override
    @Transactional(readOnly = true)
    public List<TicketPrice> findAll() {
        return Lists.newArrayList(ticketPriceRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public TicketPrice findById(Long id) {
        return ticketPriceRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public TicketPrice findTicketPriceByTicketType(TicketTypeEnum ticketTypeEnum) {
        return ticketPriceRepository.findOneTicketPriceByTicketType(ticketTypeEnum);
    }

    @Override
    public TicketPrice save(TicketPrice ticketPrice) {
        return ticketPriceRepository.save(ticketPrice);
    }

    @Override
    public void remove(TicketPrice ticketPrice) {
        ticketPriceRepository.delete(ticketPrice);
    }
}
