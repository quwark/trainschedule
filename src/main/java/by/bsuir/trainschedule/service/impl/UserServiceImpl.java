package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.User;
import by.bsuir.trainschedule.repository.UserRepository;
import by.bsuir.trainschedule.service.UserService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 10.04.2016.
 */
@Service("userService")
@Repository
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public User findByLogin(String login) {
        return userRepository.findFirstByLogin(login);
    }

    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return userRepository.findFirstByEmail(email);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void remove(User user) {
        userRepository.delete(user);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isLoginExist(String login) {
        User user = userRepository.findFirstByLogin(login);
        if(user == null) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isEmailExist(String email) {
        User user = userRepository.findFirstByEmail(email);
        if(user == null) {
            return false;
        }
        return true;
    }
}
