package by.bsuir.trainschedule.service.impl;

import by.bsuir.trainschedule.entity.Schedule;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import by.bsuir.trainschedule.repository.ScheduleRepository;
import by.bsuir.trainschedule.service.ScheduleService;
import by.bsuir.trainschedule.util.ScheduleHelper;
import com.google.common.collect.Lists;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
@Service("scheduleService")
@Repository
@Transactional
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Schedule> findAll() {
        return Lists.newArrayList(scheduleRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public Schedule findById(Long id) {
        return scheduleRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Schedule> findByDayOfWeek(DayEnum day) {
        return scheduleRepository.findByDayOfWeek(day);
    }

    @Override
    public List<Schedule> findByTime(LocalTime start, LocalTime end) {
        List<Schedule> schedules = findAll();
        return ScheduleHelper.filterByTimeRange(schedules, start, end);
    }

    @Override
    public List<Schedule> findByDayOFWeekAndTime(DayEnum day, LocalTime start, LocalTime end) {
        List<Schedule> schedules = findAll();
        schedules = ScheduleHelper.filterByDayOwWeek(schedules, day);
        return ScheduleHelper.filterByTimeRange(schedules, start, end);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Schedule> findByTrainId(Long id) {
        return scheduleRepository.findByTrainId(id);
    }

    @Override
    public Schedule save(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    @Override
    public void remove(Schedule schedule) {
        scheduleRepository.delete(schedule);
    }
}
