package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.Train;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface TrainService {
    List<Train> findAll();
    List<Train> findTrainByStations(Long startStation, Long endStation);
    List<Train> findTrainByStationsDirect(Long startStation, Long endStation);
    Train findByTrainNumber(String trainNumber);
    Train findById(Long id);
    Train save(Train user);
    void remove(Train user);
}
