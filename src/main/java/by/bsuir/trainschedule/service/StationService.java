package by.bsuir.trainschedule.service;

import by.bsuir.trainschedule.entity.Station;

import java.util.List;

/**
 * Created by Yura on 11.04.2016.
 */
public interface StationService {
    List<Station> findAll();
    Station findById(Long id);
    Station findByStationName(String name);
    Station save(Station station);
    void remove(Station station);
}
