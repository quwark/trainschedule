package by.bsuir.trainschedule.entity;

import by.bsuir.trainschedule.entity.enums.RoleEnum;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by Yura on 10.04.2016.
 */
@Entity
@Table(name = "user_data")
public class User implements Serializable {

    private Long id;
    private String login;
    private String password;
    private String email;
    private RoleEnum role;

    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "role")
    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User - id=" + id + ", login='" + login + "', password='" + password +
                "', email='" + email + "', role=" + role;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (login == null ? 0 : login.hashCode());
        result += 13 * result + (password == null ? 0 :password.hashCode());
        result += 13 * result + (email == null ? 0 : email.hashCode());
        result += 13 * result + (role == null ? 0 : role.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof User)) {
            return false;
        }
        User user = (User) obj;
        return (id == null ? user.id == null : id.equals(user.id)) &&
                (login == null ? user.login == null : login.equals(user.login)) &&
                (password == null ? user.password == null : password.equals(user.password)) &&
                (email == null ? user.email == null : email.equals(user.email)) &&
                (role == null ? user.role == null : role == user.role);
    }
}
