package by.bsuir.trainschedule.entity;

import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yura on 10.04.2016.
 */
@Entity
@Table(name = "ticket_price")
public class TicketPrice implements Serializable {

    private Long id;
    private TicketTypeEnum ticketType;
    private int startPrice;
    private int unitPrice;

    public TicketPrice() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "ticket_type")
    @Enumerated(EnumType.STRING)
    public TicketTypeEnum getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketTypeEnum ticketType) {
        this.ticketType = ticketType;
    }

    @Column(name = "start_price")
    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    @Column(name = "unit_price")
    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "TicketPrice - id=" + id + ", ticketType='" + ticketType + "', startPrice=" + startPrice +
                ", unitPrice=" + unitPrice;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (ticketType == null ? 0 : ticketType.hashCode());
        result += 13 * result + startPrice;
        result += 13 * result + unitPrice;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof TicketPrice)) {
            return false;
        }

        TicketPrice ticketPrice = (TicketPrice) obj;
        return (id == null ? ticketPrice.id == null : id.equals(ticketPrice.id)) &&
                (ticketType == null ? ticketPrice.ticketType == null : ticketType.equals(ticketPrice.ticketType)) &&
                startPrice == ticketPrice.startPrice &&
                unitPrice == ticketPrice.unitPrice;
    }
}
