package by.bsuir.trainschedule.entity.enums;

/**
 * Created by Yura on 11.04.2016.
 */
public enum  TrainStatusEnum {
    STOP,
    WORK
}
