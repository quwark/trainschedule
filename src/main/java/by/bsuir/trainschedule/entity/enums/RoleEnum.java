package by.bsuir.trainschedule.entity.enums;

/**
 * Created by Yura on 10.04.2016.
 */
public enum RoleEnum {
    ANONYMOUS,
    USER,
    ADMIN;
}
