package by.bsuir.trainschedule.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Yura on 10.04.2016.
 */
@Entity
@Table(name = "route")
public class Route implements Serializable {

    private Long id;
    private Long trainId;
    private Long stationId;
    private int timeShift;

    public Route() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "train_id")
    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    @Column(name = "station_id")
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    @Column(name = "time_shift")
    public int getTimeShift() {
        return timeShift;
    }

    public void setTimeShift(int timeShift) {
        this.timeShift = timeShift;
    }

    @Override
    public String toString() {
        return "Route - id=" + id + ", train=" + trainId + ", station=" + stationId + ", timeShift=" + timeShift;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (trainId == null ? 0 : trainId.hashCode());
        result += 13 * result + (stationId == null ? 0 : stationId.hashCode());
        result += 13 * result + timeShift;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Route)) {
            return false;
        }
        Route route = (Route) obj;
        return (id == null ? route.id == null : id.equals(route.id)) &&
                (trainId == null ? route.trainId == null : trainId.equals(route.trainId)) &&
                (stationId == null ? route.stationId == null : stationId.equals(route.stationId)) &&
                (timeShift == route.timeShift);
    }
}
