package by.bsuir.trainschedule.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yura on 10.04.2016.
 */

@Entity
@Table(name = "train")
public class Train implements Serializable {

    private Long id;
    private String trainNumber;
    private String name;

    public Train() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "train_number")
    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Train - id=" + id + ", trainNumber='" + trainNumber + "', name='" + name + '\'';
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (trainNumber == null ? 0 : trainNumber.hashCode());
        result += 13 * result + (name == null ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Train)) {
            return false;
        }
        Train train = (Train) obj;
        return (id == null ? train.id == null : id.equals(train.id)) &&
                (trainNumber == null ? train.trainNumber == null : trainNumber.equals(train.trainNumber)) &&
                (name == null ? train.name == null : name.equals(train.name));
    }
}
