package by.bsuir.trainschedule.entity;

import by.bsuir.trainschedule.entity.enums.DayEnum;
import by.bsuir.trainschedule.entity.enums.TrainStatusEnum;
import org.hibernate.annotations.Type;
import org.joda.time.LocalTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Yura on 10.04.2016.
 */
@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

    private Long id;
    private Long trainId;
    private DayEnum dayOfWeek;
    private LocalTime departureTime;
    private TrainStatusEnum trainStatus;

    public Schedule() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "train_id")
    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    @Column(name = "day_of_week")
    @Enumerated(EnumType.STRING)
    public DayEnum getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayEnum dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Column(name = "departure_time")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsTimestamp")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    @Column(name = "active")
    @Enumerated(EnumType.ORDINAL)
    public TrainStatusEnum getTrainStatus() {
        return trainStatus;
    }

    public void setTrainStatus(TrainStatusEnum trainStatus) {
        this.trainStatus = trainStatus;
    }

    @Override
    public String toString() {
        return "Schedule - id=" + id + ", train=" + trainId + ", dayOfWeek=" + dayOfWeek + ", localTime=" + departureTime +
                ", trainStatus=" + trainStatus;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (trainId == null ? 0 : trainId.hashCode());
        result += 13 * result + (dayOfWeek == null ? 0 : dayOfWeek.hashCode());
        result += 13 * result + (departureTime == null ? 0 : departureTime.hashCode());
        result += 13 * result + (trainStatus == null ? 0 : trainStatus.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Schedule)) {
            return false;
        }
        Schedule schedule = (Schedule) obj;
        return (id == null ? schedule.id == null : id.equals(schedule.id)) &&
                (trainId == null ? schedule.trainId == null : trainId.equals(schedule.trainId)) &&
                (dayOfWeek == null ? schedule.dayOfWeek == null : dayOfWeek == schedule.dayOfWeek) &&
                (departureTime == null ? schedule.departureTime == null : departureTime.equals(schedule.departureTime)) &&
                (trainStatus == null ? schedule.trainStatus == null : trainStatus == schedule.trainStatus);
    }
}
