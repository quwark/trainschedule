package by.bsuir.trainschedule.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yura on 10.04.2016.
 */

@Entity
@Table(name = "station")
public class Station implements Serializable {

    private Long id;
    private String name;

    public Station() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Station -  id=" + id + ", name='" + name;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (name == null ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Station)) {
            return false;
        }
        Station station = (Station) obj;
        return (id == null ? station.id == null : id.equals(station.id)) &&
                (name == null ? station.name == null : name.equals(station.name));
    }
}
