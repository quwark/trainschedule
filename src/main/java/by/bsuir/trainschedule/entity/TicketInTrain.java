package by.bsuir.trainschedule.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Yura on 10.04.2016.
 */

@Entity
@Table(name = "ticket_in_train")
public class TicketInTrain implements Serializable {

    private Long id;
    private Long trainId;
    private Long ticketId;
    private int count;

    public TicketInTrain() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "train_id")
    public Long getTrainId() {
        return trainId;
    }

    public void setTrainId(Long trainId) {
        this.trainId = trainId;
    }

    @Column(name = "ticket_id")
    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    @Column(name = "ticket_count")
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "TicketInTrain - id=" + id + ", train=" + trainId + ", ticketPrice=" + ticketId + ", count=" + count;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + (id == null ? 0 : id.hashCode());
        result += 13 * result + (trainId == null ? 0 : trainId.hashCode());
        result += 13 * result + (ticketId == null ? 0 : ticketId.hashCode());
        result += 13 * result + count;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof TicketInTrain)) {
            return false;
        }

        TicketInTrain ticketInTrain = (TicketInTrain) obj;
        return (id == null ? ticketInTrain.id == null : id.equals(ticketInTrain.id)) &&
                (trainId == null ? ticketInTrain.trainId == null : trainId.equals(ticketInTrain.trainId)) &&
                (ticketId == null ? ticketInTrain.ticketId == null : ticketId.equals(ticketInTrain.ticketId)) &&
                count == ticketInTrain.count;
    }
}
