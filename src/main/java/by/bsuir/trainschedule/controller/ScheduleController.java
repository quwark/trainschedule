package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.Schedule;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import by.bsuir.trainschedule.service.ScheduleService;
import com.sun.istack.internal.logging.Logger;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Yura on 22.04.2016.
 */
@Controller
@RequestMapping(value = "/schedule")
public class ScheduleController {

    private static final Logger logger = Logger.getLogger(ScheduleController.class);

    @Autowired
    private ScheduleService scheduleService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<Schedule> getScheduleById(@PathVariable Long id) {
        Schedule schedule = scheduleService.findById(id);
        if(schedule == null) {
            logger.info("Schedule not found with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(schedule, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Schedule>> getAllSchedules(@RequestParam(required = false, value = "day") DayEnum day,
                                                      @RequestParam(required = false, value = "start") LocalTime start,
                                                      @RequestParam(required = false, value = "end") LocalTime end) {
        System.out.println("day = " + day + "|start = " + start + "|end = " + end);
        List<Schedule> schedules;
        if(day == null && start == null && end == null) {
            schedules = scheduleService.findAll();
        }else {
            schedules = scheduleService.findByDayOFWeekAndTime(day, start, end);
        }
        if(schedules.isEmpty()) {
            logger.info("Schedules not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(schedules, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Schedule> create(@RequestBody Schedule schedule) {
        return new ResponseEntity<>(scheduleService.save(schedule), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody Schedule schedule, @PathVariable Long id) {
        Schedule currentSchedule = scheduleService.findById(id);
        if(currentSchedule == null) {
            logger.info("Schedule can't be update with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        scheduleService.save(schedule);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        Schedule currentSchedule = scheduleService.findById(id);
        if(currentSchedule == null) {
            logger.info("Schedule can't be remove with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        scheduleService.remove(currentSchedule);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/day/{day}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Schedule>> getAllByDayOfWeek(@PathVariable DayEnum day) {
        List<Schedule> schedules = scheduleService.findByDayOfWeek(day);
        if(schedules.isEmpty()) {
            logger.info("Schedules not found on day: " + day);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(schedules, HttpStatus.OK);
    }

    @RequestMapping(value = "/train/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Schedule>> getAllByTrainId(@PathVariable Long id) {
        List<Schedule> schedules = scheduleService.findByTrainId(id);
        if(schedules.isEmpty()) {
            logger.info("Schedules not found with train id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(schedules, HttpStatus.OK);
    }

}
