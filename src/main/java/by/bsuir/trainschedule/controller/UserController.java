package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.User;
import by.bsuir.trainschedule.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Yura on 20.04.2016.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final static Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        User user = userService.findById(id);
        if(user == null) {
            logger.info("Train not found with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.findAll();
        if(users.isEmpty()) {
            logger.info("Trains not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<User> create(@RequestBody User user) {
        return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody User user, @PathVariable Long id) {
        User currentUser = userService.findById(id);
        if(currentUser == null) {
            logger.info("Train can't be update with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        user.setId(id);
        userService.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        User currentUser = userService.findById(id);
        if(currentUser == null) {
            logger.info("Train can't be remove with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        userService.remove(currentUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/login/{login}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<User> getByLogin(@PathVariable String login) {
        User currentUser = userService.findByLogin(login);
        if(currentUser == null) {
            logger.info("Train can't be found with login: " + login);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/email/{email}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<User> getByEmail(@PathVariable String email) {
        User currentUser = userService.findByEmail(email);
        if(currentUser == null) {
            logger.info("Train can't be found with email: " + email);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }
    /*@RequestMapping(value = "/email/check{email}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Boolean> checkIfEmailExist(@PathVariable String email) {
        return new ResponseEntity<>(userService.isEmailExist(email), HttpStatus.OK);
    }

    @RequestMapping(value = "/login/check/{login}", method = RequestMethod.GET)
    @ResponseBody
    public boolean checkIfLoginExist(@PathVariable String login) {
        return userService.isLoginExist(login);
    }*/
}
