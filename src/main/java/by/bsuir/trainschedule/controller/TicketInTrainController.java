package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.TicketInTrain;
import by.bsuir.trainschedule.service.TicketInTrainService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by Yura on 22.04.2016.
 */
@Controller
@RequestMapping(value = "/ticketInTrain")
public class TicketInTrainController {

    private static final Logger logger = Logger.getLogger(TicketInTrain.class);

    @Autowired
    private TicketInTrainService ticketInTrainService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<TicketInTrain> getUserById(@PathVariable Long id) {
        TicketInTrain ticketInTrain = ticketInTrainService.findById(id);
        if(ticketInTrain == null) {
            logger.info("TicketInTrain not found with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ticketInTrain, HttpStatus.OK);
    }

    @RequestMapping(value = "/train/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<List<TicketInTrain>> getTicketInTrainByTrainId(@PathVariable Long id) {
        List<TicketInTrain> ticketInTrain = ticketInTrainService.findByTrainId(id);
        System.out.println(ticketInTrain.get(0));
        if(ticketInTrain.isEmpty()) {
            logger.info("TicketInTrain not found with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ticketInTrain, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<TicketInTrain>> getAllUsers() {
        List<TicketInTrain> ticketInTrains = ticketInTrainService.findAll();
        if(ticketInTrains.isEmpty()) {
            logger.info("TicketInTrains not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ticketInTrains, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TicketInTrain> create(@RequestBody TicketInTrain ticketInTrain) {
        return new ResponseEntity<>(ticketInTrainService.save(ticketInTrain), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody TicketInTrain ticketInTrain, @PathVariable Long id) {
        TicketInTrain currentTicketInTrain = ticketInTrainService.findById(id);
        if(currentTicketInTrain == null) {
            logger.info("TicketInTrain can't be update id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ticketInTrainService.save(ticketInTrain);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        TicketInTrain currentTicketInTrain = ticketInTrainService.findById(id);
        if(currentTicketInTrain == null) {
            logger.info("TicketInTrain can't be remove with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ticketInTrainService.remove(currentTicketInTrain);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
