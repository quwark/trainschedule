package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.Station;
import by.bsuir.trainschedule.service.StationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Yura on 22.04.2016.
 */
@Controller
@RequestMapping(value = "/station")
public class StationController {

    private static final Logger logger = Logger.getLogger(StationController.class);

    @Autowired
    private StationService stationService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Station> getUserById(@PathVariable Long id) {
        Station station = stationService.findById(id);
        if(station == null) {
            logger.info("Station not found with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(station, HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Station> getStationByName(@PathVariable String name) {
        Station station = stationService.findByStationName(name);
        if(station == null) {
            logger.info("Station not found with  name: " + name);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(station, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Station>> getAllUsers() {
        List<Station> stations = stationService.findAll();
        if(stations.isEmpty()) {
            logger.info("Stations not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(stations, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Station> create(@RequestBody Station station) {
        return new ResponseEntity<>(stationService.save(station), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody Station station, @PathVariable Long id) {
        Station currentStation = stationService.findById(id);
        if(currentStation == null) {
            logger.info("Station can't be update with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        stationService.save(station);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        Station currentStation = stationService.findById(id);
        if(currentStation == null) {
            logger.info("Station can't be remove with  id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        stationService.remove(currentStation);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
