package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.TicketPrice;
import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;
import by.bsuir.trainschedule.service.TicketPriceService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Yura on 22.04.2016.
 */
@Controller
@RequestMapping(value = "/ticketPrice")
public class TicketPriceController {

    private static final Logger logger = Logger.getLogger(TicketPriceController.class);

    @Autowired
    private TicketPriceService ticketPriceService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<TicketPrice> getUserById(@PathVariable Long id) {
        TicketPrice ticketPrice = ticketPriceService.findById(id);
        if(ticketPrice == null) {
            logger.info("TicketPrice not found with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ticketPrice, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<TicketPrice>> getAllUsers() {
        List<TicketPrice> ticketPrices = ticketPriceService.findAll();
        if(ticketPrices.isEmpty()) {
            logger.info("TicketPrices not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(ticketPrices, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TicketPrice> create(@RequestBody TicketPrice ticketPrice) {
        return new ResponseEntity<>(ticketPriceService.save(ticketPrice), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody TicketPrice ticketPrice, @PathVariable Long id) {
        TicketPrice currentTicketPrice = ticketPriceService.findById(id);
        if(currentTicketPrice == null) {
            logger.info("TicketPrice can't be update with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ticketPriceService.save(ticketPrice);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        TicketPrice currentTicketPrice = ticketPriceService.findById(id);
        if(currentTicketPrice == null) {
            logger.info("TicketPrice can't be remove with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        ticketPriceService.remove(currentTicketPrice);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "ticketType/{type}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TicketPrice> getTicketPriceByType(@PathVariable TicketTypeEnum type) {
        TicketPrice currentTicketPrice = ticketPriceService.findTicketPriceByTicketType(type);
        if(currentTicketPrice == null){
            logger.info("TicketPrice not found with type: " + type);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else {
            return new ResponseEntity<>(currentTicketPrice, HttpStatus.OK);
        }
    }
}
