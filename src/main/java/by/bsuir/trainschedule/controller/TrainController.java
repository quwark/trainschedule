package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.dto.TrainDTO;
import by.bsuir.trainschedule.entity.*;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import by.bsuir.trainschedule.entity.enums.TicketTypeEnum;
import by.bsuir.trainschedule.entity.enums.TrainStatusEnum;
import by.bsuir.trainschedule.service.*;
import org.apache.log4j.Logger;
import org.joda.time.LocalTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Yura on 22.04.2016.
 */
@Controller
@RequestMapping(value = "/train")
public class TrainController {

    private static final Logger logger = Logger.getLogger(TrainController.class);

    @Autowired
    private TrainService trainService;
    @Autowired
    private RouteService routeService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private TicketInTrainService ticketInTrainService;
    @Autowired
    private StationService stationService;
    @Autowired
    private TicketPriceService ticketPriceService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody()
    public ResponseEntity<Train> getUserById(@PathVariable Long id) {
        Train train = trainService.findById(id);
        if(train == null) {
            logger.info("Train not found with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(train, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Train>> getAllUsers() {
        List<Train> trains = trainService.findAll();
        if(trains.isEmpty()) {
            logger.info("Trains not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(trains, HttpStatus.OK);
    }

    @RequestMapping(value = "/stations", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Train>> getTrainByStationsId(@RequestParam(value = "from") Long from,
                                                            @RequestParam(value = "to") Long to) {
        System.out.println("From = " + from + "|To = " + to);
        List<Train> trains = trainService.findTrainByStationsDirect(from, to);
        if(trains.isEmpty()) {
            logger.info("Trains not found with start station id: " + from + " and end station id: " + from);
            return new ResponseEntity<>(trains, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(trains, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Train> create(@RequestBody Train train) {
        return new ResponseEntity<>(trainService.save(train), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody Train train, @PathVariable Long id) {
        Train currentTrain = trainService.findById(id);
        if(currentTrain == null) {
            logger.info("Train can't be update with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        try{
            trainService.save(train);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        Train currentTrain = trainService.findById(id);
        if(currentTrain == null) {
            logger.info("Train can't be remove with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        trainService.remove(currentTrain);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/dto", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> addTrain(@RequestBody TrainDTO trainDTO) {
        try {
            Train temp = trainService.findByTrainNumber(trainDTO.getTrain().getTrainNumber());
            if(temp == null) {
                Train train = trainDTO.getTrain();
                trainService.save(train);
                Station departureStation = stationService.findByStationName(trainDTO.getDepartureStation());
                Station arrivalStation = stationService.findByStationName(trainDTO.getArrivalStation());
                Route tempRoute = new Route();
                tempRoute.setStationId(departureStation.getId());
                tempRoute.setTrainId(train.getId());
                tempRoute.setTimeShift(0);
                routeService.save(tempRoute);
                tempRoute.setId(null);
                tempRoute.setStationId(arrivalStation.getId());

                DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm");
                LocalTime departure = formatter.parseLocalTime(trainDTO.getDepartureTime());
                LocalTime arrival = formatter.parseLocalTime(trainDTO.getArrivalTime());

                Seconds seconds = Seconds.secondsBetween(departure, arrival);
                tempRoute.setTimeShift(seconds.getSeconds());
                routeService.save(tempRoute);

                Schedule schedule = new Schedule();
                schedule.setDepartureTime(departure);
                schedule.setTrainId(train.getId());
                schedule.setTrainStatus(TrainStatusEnum.WORK);
                DayEnum[] days = DayEnum.values();
                for(Integer day : trainDTO.getDays()) {
                    schedule.setId(null);
                    schedule.setDayOfWeek(days[day]);
                    scheduleService.save(schedule);
                }
                TicketInTrain ticketInTrain = new TicketInTrain();
                ticketInTrain.setTrainId(train.getId());
                List<TicketPrice> ticketPrices = ticketPriceService.findAll();

                ticketInTrain.setCount(trainDTO.getThirdClassTicketCount());
                ticketInTrain.setTicketId(ticketPriceService.findTicketPriceByTicketType(TicketTypeEnum.THIRD_CLASS).getId());
                ticketInTrainService.save(ticketInTrain);

                ticketInTrain.setId(null);
                ticketInTrain.setCount(trainDTO.getSecondClassTicketCount());
                ticketInTrain.setTicketId(ticketPriceService.findTicketPriceByTicketType(TicketTypeEnum.SECOND_CLASS).getId());
                ticketInTrainService.save(ticketInTrain);

                ticketInTrain.setId(null);
                ticketInTrain.setCount(trainDTO.getFirstClassTicketCount());
                ticketInTrain.setTicketId(ticketPriceService.findTicketPriceByTicketType(TicketTypeEnum.FIRST_CLASS).getId());
                ticketInTrainService.save(ticketInTrain);

                ticketInTrain.setId(null);
                ticketInTrain.setCount(trainDTO.getBerthTicketCount());
                ticketInTrain.setTicketId(ticketPriceService.findTicketPriceByTicketType(TicketTypeEnum.BERTH).getId());
                ticketInTrainService.save(ticketInTrain);

                ticketInTrain.setId(null);
                ticketInTrain.setCount(trainDTO.getSeatTicketCount());
                ticketInTrain.setTicketId(ticketPriceService.findTicketPriceByTicketType(TicketTypeEnum.SEAT).getId());
                ticketInTrainService.save(ticketInTrain);

                return new ResponseEntity<>(HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
