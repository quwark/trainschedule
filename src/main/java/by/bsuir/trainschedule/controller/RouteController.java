package by.bsuir.trainschedule.controller;

import by.bsuir.trainschedule.entity.Route;
import by.bsuir.trainschedule.service.RouteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Yura on 21.04.2016.
 */
@Controller
@RequestMapping(value = "/route")
public class RouteController {

    private static final Logger logger = Logger.getLogger(RouteController.class);

    @Autowired
    private RouteService routeService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Route> getRouteById(@PathVariable Long id) {
        Route route = routeService.findById(id);
        if(route == null) {
            logger.info("Route not found with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(route, HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Route>> getAllRoutes() {
        List<Route> rotes = routeService.findAll();
        if(rotes.isEmpty()) {
            logger.info("Routes not found");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(rotes, HttpStatus.OK);
    }

    @RequestMapping(value = "/train/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Route>> getAllByTrainId(@PathVariable Long id) {
        List<Route> routes = routeService.findByTrainId(id);
        if(routes.isEmpty()) {
            logger.info("Routes not found with train id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(routes, HttpStatus.OK);
    }

    @RequestMapping(value = "/station/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Route>> getAllByStationId(@PathVariable Long id) {
        List<Route> routes = routeService.findByStationId(id);
        if(routes.isEmpty()) {
            logger.info("Rotes not found with station id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(routes, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Route> create(@RequestBody Route route) {
        Route newRoute = null;
        System.out.println(route);
        try {
            newRoute  = routeService.save(route);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(newRoute, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@RequestBody Route route, @PathVariable Long id) {
        Route currentRoute = routeService.findById(id);
        if(currentRoute == null) {
            logger.info("Route can't be update with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        routeService.save(route);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Void> remove(@PathVariable Long id) {
        Route currentRoute = routeService.findById(id);
        if(currentRoute == null) {
            logger.info("Route can't be remove with id: " + id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        routeService.remove(currentRoute);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/trainAndStation/{trainId}/{stationId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Route> getRouteById(@PathVariable Long trainId, @PathVariable Long stationId) {
        Route route = routeService.findByTrainAndStationId(trainId, stationId);
        if(route == null) {
            logger.info("Route not found with trainId: " + trainId + " and stationId: " + stationId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(route, HttpStatus.OK);
    }
}
