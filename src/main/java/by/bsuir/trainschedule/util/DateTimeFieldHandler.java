package by.bsuir.trainschedule.util;

import org.exolab.castor.mapping.GeneralizedFieldHandler;
import org.exolab.castor.mapping.ValidityException;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Properties;

/**
 * Created by Yura on 22.04.2016.
 */
public class DateTimeFieldHandler extends GeneralizedFieldHandler {
    private static String localFormatPattern;

    @Override
    public Object convertUponGet(Object o) {
        LocalTime localTime = (LocalTime) o;
        return format(localTime);
    }

    @Override
    public Object convertUponSet(Object o) {
        String localTimeString = (String) o;
        return parse(localTimeString);
    }



    @Override
    public Class<LocalTime> getFieldType() {
        return LocalTime.class;
    }

    @Override
    public void setConfiguration(Properties config) throws ValidityException {
        localFormatPattern = config.getProperty("date-format");
    }

    protected static String format(final LocalTime localTime) {
        String localTimeString = "";

        if(localTime != null) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(localFormatPattern);
            localTimeString = dateTimeFormatter.print(localTime);
        }
        return localTimeString;
    }

    protected static LocalTime parse(final String localTimeString) {
        LocalTime localTime = new LocalTime();
        System.out.println("t = " + localTimeString);
        if(localTimeString != null) {
            DateTimeFormatter localTimeFormatter = DateTimeFormat.forPattern(localFormatPattern);
            localTime = localTimeFormatter.parseLocalTime(localTimeString);
        }
        return localTime;
    }
}
