package by.bsuir.trainschedule.util;

import by.bsuir.trainschedule.entity.Schedule;
import by.bsuir.trainschedule.entity.enums.DayEnum;
import org.joda.time.LocalTime;

import java.util.List;

/**
 * Created by Yura on 26.04.2016.
 */
public class ScheduleHelper {

    private ScheduleHelper() {

    }

    public static List<Schedule> filterByTimeRange(List<Schedule> schedules, LocalTime start, LocalTime end) {
        schedules.removeIf(schedule -> {
            LocalTime departureTime = schedule.getDepartureTime();
            if(start == null && end == null) {
                return false;
            }else if(start == null) {
                return !(departureTime.compareTo(end) <= 0);
            }else if(end == null) {
                return !(departureTime.compareTo(start) >= 0);
            }
            return !(departureTime.compareTo(start) >= 0 && departureTime.compareTo(end) <= 0);
        });
        return schedules;
    }

    public static List<Schedule> filterByDayOwWeek(List<Schedule> schedules, DayEnum day) {
        if(day == null) {
            return schedules;
        }
        schedules.removeIf(schedule -> schedule.getDayOfWeek() != day);
        return schedules;
    }
}
